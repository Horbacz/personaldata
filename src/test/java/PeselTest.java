import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class PeselTest {

    @Test
    void shouldThrowException_whenPesel_hasNot11Digits() {
        //given
        String peselWith12Digits = "940307134962";
        String peselWith9Digits = "9403071349";

        //when + then
        assertThatThrownBy(() -> new Pesel(peselWith12Digits))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Pesel should have eleven signs");

        assertThatThrownBy(() -> new Pesel(peselWith9Digits))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Pesel should have eleven signs");
    }

    @Test
    void shouldThrowException_whenPesel_ControlNumberIsNotValid(){
        //given
        String peselWithWrongControlNumber = "94030713497";

        //when + then
        assertThatThrownBy(() -> new Pesel(peselWithWrongControlNumber))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Pesel control number is not valid.");
    }

    @Test
    void shouldHaveCorrectMaleSex() {
        //given
        String pesel = "94030713496";

        //when
        Pesel person = new Pesel(pesel);

        //then
        assertThat(person.getSex()).isEqualTo(Pesel.Sex.MALE);
    }

    @Test
    void shouldHaveCorrectFemaleSex() {
        //given
        String pesel = "99011357403";

        //when
        Pesel person = new Pesel(pesel);

        //then
        assertThat(person.getSex()).isEqualTo(Pesel.Sex.FEMALE);
    }

    @Test
    void shouldHaveCorrectDataOfBirth() {
        //given
        String firstPesel = "94030713496";
        String secondPesel = "33462314712";
        String thirdPesel = "43251246719";
        String fourthPesel = "56872164715";
        String fifthPesel = "12700308614";

        //when
        Pesel firstPerson = new Pesel(firstPesel);
        Pesel secondPerson = new Pesel(secondPesel);
        Pesel thirdPerson = new Pesel(thirdPesel);
        Pesel fourthPerson = new Pesel(fourthPesel);
        Pesel fifthPerson = new Pesel(fifthPesel);

        //then
        assertThat(firstPerson.getDateOfBirth()).isEqualTo("1994-03-07");
        assertThat(secondPerson.getDateOfBirth()).isEqualTo("2133-06-23");
        assertThat(thirdPerson.getDateOfBirth()).isEqualTo("2043-05-12");
        assertThat(fourthPerson.getDateOfBirth()).isEqualTo("1856-07-21");
        assertThat(fifthPerson.getDateOfBirth()).isEqualTo("2212-10-03");
    }
}
