import java.time.LocalDate;

public class Pesel {

    private String pesel;
    private Sex sex;
    private LocalDate dateOfBirth;

    Pesel(String pesel){
        this.pesel = pesel;
        validateLength(pesel);
        validateControlNumber(pesel);
        this.sex = calculateSexInfo(pesel);
        this.dateOfBirth = calculateDateOfBirth(pesel);
    }

    private void validateLength(String pesel) {
        if (pesel.length() != 11 ){
            throw new IllegalArgumentException("Pesel should have eleven signs");
        }
    }

    private void validateControlNumber(String pesel) {
        int firstSign = Integer.parseInt(pesel.substring(0, 1));
        int secondSign = Integer.parseInt(pesel.substring(1, 2));
        int thirdSign = Integer.parseInt(pesel.substring(2, 3));
        int fourthSign = Integer.parseInt(pesel.substring(3, 4));
        int fifthSign = Integer.parseInt(pesel.substring(4, 5));
        int sixthSign = Integer.parseInt(pesel.substring(5, 6));
        int seventhSign = Integer.parseInt(pesel.substring(6, 7));
        int eighthSign = Integer.parseInt(pesel.substring(7, 8));
        int ninethSign = Integer.parseInt(pesel.substring(8, 9));
        int tenthSign = Integer.parseInt(pesel.substring(9, 10));
        int controlNumber = Integer.parseInt(pesel.substring(10, 11));
        int peselNumberCombination = firstSign * 1 + secondSign * 3 + thirdSign * 7 + fourthSign * 9 + fifthSign * 1 + sixthSign * 3 + seventhSign * 7 + eighthSign * 9 + ninethSign * 1 + tenthSign * 3;
        int calcControlNumber = 0;
        if (peselNumberCombination > 99) {
            calcControlNumber = Integer.parseInt(String.valueOf(peselNumberCombination).substring(2, 3));
            calcControlNumber = 10 - calcControlNumber;
        }
        if (peselNumberCombination <= 99 && peselNumberCombination > 10) {
            calcControlNumber = Integer.parseInt(String.valueOf(peselNumberCombination).substring(1, 2));
            calcControlNumber = 10 - calcControlNumber;
        }
        if (peselNumberCombination <= 10) {
            calcControlNumber = Integer.parseInt(String.valueOf(peselNumberCombination).substring(0, 1));
            calcControlNumber = 10 - calcControlNumber;
        }
        if (calcControlNumber != controlNumber) {
            throw new IllegalArgumentException("Pesel control number is not valid.");
        }
    }

    public enum Sex {
        MALE, FEMALE;
    }

    private Sex calculateSexInfo(String pesel) {
        int sexSign = Integer.parseInt(pesel.substring(9, 10));
        if(sexSign % 2 == 0) {
            return Sex.FEMALE;
        }else {
            return Sex.MALE;
        }
    }

    private Integer retrieveYearOfBirth(String pesel) {
        int yearOfBirth = 0;
        String eraSign = pesel.substring(2, 3);
        String TwoLastYearCooridates = pesel.substring(0, 2);;
        if ("8".equals(eraSign) || "9".equals(eraSign)) {
            yearOfBirth = Integer.parseInt("18" + TwoLastYearCooridates);
            return yearOfBirth;
        }else if ("0".equals(eraSign) || "1".equals(eraSign)) {
            yearOfBirth = Integer.parseInt("19" + TwoLastYearCooridates);
            return yearOfBirth;
        }else if ("2".equals(eraSign) || "3".equals(eraSign)) {
            yearOfBirth = Integer.parseInt("20"+ TwoLastYearCooridates);
            return yearOfBirth;
        }else if ("4".equals(eraSign) || "5".equals(eraSign)) {
            yearOfBirth = Integer.parseInt("21" + TwoLastYearCooridates);
            return yearOfBirth;
        }else if ("6".equals(eraSign) || "7".equals(eraSign)) {
            yearOfBirth = Integer.parseInt("22" + TwoLastYearCooridates);
            return yearOfBirth;
        }
        return yearOfBirth;

    }

    private Integer retrieveMonthOfBirth(String pesel) {
        String monthOfBirthFromPesel = pesel.substring(2, 4);
        String secondMonthSign = monthOfBirthFromPesel.substring(1, 2);
        String eraSign = pesel.substring(2, 3);
        int monthOfBirth = 0;
        if ("8".equals(eraSign)) {
            monthOfBirth = Integer.parseInt("0" + secondMonthSign);
            return monthOfBirth;
        }else if("9".equals(eraSign)){
            monthOfBirth = Integer.parseInt("1" + secondMonthSign);
            return monthOfBirth;
        }else if("0".equals(eraSign)){
            monthOfBirth = Integer.parseInt("0" + secondMonthSign);
            return monthOfBirth;
        }else if("1".equals(eraSign)){
            monthOfBirth = Integer.parseInt("1" + secondMonthSign);
            return monthOfBirth;
        }else if("2".equals(eraSign)){
            monthOfBirth = Integer.parseInt("0" + secondMonthSign);
            return monthOfBirth;
        }else if("3".equals(eraSign)){
            monthOfBirth = Integer.parseInt("1" + secondMonthSign);
            return monthOfBirth;
        }else if("4".equals(eraSign)){
            monthOfBirth = Integer.parseInt("0" + secondMonthSign);
            return monthOfBirth;
        }else if("5".equals(eraSign)){
            monthOfBirth = Integer.parseInt("1" + secondMonthSign);
            return monthOfBirth;
        }else if("6".equals(eraSign)){
            monthOfBirth = Integer.parseInt("0" + secondMonthSign);
            return monthOfBirth;
        }else if("7".equals(eraSign)){
            monthOfBirth = Integer.parseInt("1" + secondMonthSign);
            return monthOfBirth;
        }
        return monthOfBirth;
    }

    private LocalDate calculateDateOfBirth(String pesel) {
        int dayOfBirth = Integer.parseInt(pesel.substring(4, 6));
        int yearOfBirth = retrieveYearOfBirth(pesel);
        int monthOfBirth = retrieveMonthOfBirth(pesel);
        return dateOfBirth = LocalDate.of(yearOfBirth,monthOfBirth,dayOfBirth);
    }

    public String getPesel() {
        return pesel;
    }

    public Sex getSex() {
        return sex;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
}
