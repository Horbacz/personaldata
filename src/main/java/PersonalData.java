import java.time.format.DateTimeFormatter;

public class PersonalData {
    public static void main(String[] args) {
        Pesel osoba = new Pesel("94030713496");
        System.out.print("Pesel is " + osoba.getPesel());
        System.out.print("; Sex is: " + osoba.getSex());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY");
        System.out.println("; Date of birth is: " + formatter.format(osoba.getDateOfBirth()));
    }
}
